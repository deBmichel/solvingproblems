#!/bin/python3

#url: https://www.hackerrank.com/challenges/grading/problem

import math
import os
import random
import re
import sys

#
# Complete the 'gradingStudents' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY grades as parameter.
#

def gradingStudents(grades, grades_count):
    i, ret = 0, []
    while i < grades_count:
        grade = grades[i]
        if grade < 38:
            ret.append(grade)
        else:
            #IMPLICIT 0 : Be carefull cowboy. Imagine 70 -> 75 : 3:)
            while grade % 5 != 0:
                grade += 1
            if ( grade - grades[i] ) < 3:
                ret.append(grade)
            else:
                ret.append(grades[i])
        i += 1
    return ret

if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    grades_count = int(input().strip())

    grades = []

    for _ in range(grades_count):
        grades_item = int(input().strip())
        grades.append(grades_item)

    result = gradingStudents(grades, grades_count)
    print(*result)

    #fptr.write('\n'.join(map(str, result)))
    #fptr.write('\n')

    #fptr.close()
