#!/bin/python3

#url: https://www.hackerrank.com/challenges/apple-and-orange/problem

import math
import os
import random
import re
import sys

# Complete the countApplesAndOranges function below.
def countApplesAndOranges(s, t, a, b, apples, oranges):
    iappls, limitappls, applesinhouse = 0, len(apples), 0
    while iappls < limitappls:
        if a + apples[iappls] >= s and a + apples[iappls] <= t:
            applesinhouse += 1
        iappls += 1
    iorngs, limitorngs, orngsinhouse = 0, len(oranges), 0
    while iorngs < limitorngs:
        if b + oranges[iorngs] >= s and b + oranges[iorngs] <= t:
            orngsinhouse += 1
        iorngs += 1
    print(f'{applesinhouse}\n{orngsinhouse}')


if __name__ == '__main__':
    st = input().split()

    s = int(st[0])

    t = int(st[1])

    ab = input().split()

    a = int(ab[0])

    b = int(ab[1])

    mn = input().split()

    m = int(mn[0])

    n = int(mn[1])

    apples = list(map(int, input().rstrip().split()))

    oranges = list(map(int, input().rstrip().split()))

    countApplesAndOranges(s, t, a, b, apples, oranges)
