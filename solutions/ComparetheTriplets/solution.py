#!/bin/python3

#url: https://www.hackerrank.com/challenges/compare-the-triplets/problem?h_r=profile

import math
import os
import random
import re
import sys

# Complete the compareTriplets function below.
def compareTriplets(a, b):
    i, lenarrs, retAB = 0, len(a), [0,0]
    while i < lenarrs:
        if a[i] > b[i]:
            retAB[0] += 1
        elif a[i] < b[i]:
            retAB[1] += 1
        i += 1
    return retAB

if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = list(map(int, input().rstrip().split()))

    b = list(map(int, input().rstrip().split()))

    result = compareTriplets(a, b)

    print(result)

    #fptr.write(' '.join(map(str, result)))
    #fptr.write('\n')

    #fptr.close()

