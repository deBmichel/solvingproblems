#url: https://www.hackerrank.com/challenges/plus-minus/problem

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the plusMinus function below.
def plusMinus(arr, lenarr):
    i, upcero, downcero, zero = 0, 0, 0, 0
    while i < lenarr:
        if arr[i] > 0:
            upcero += 1
        elif arr[i] < 0:
            downcero += 1
        i += 1
    zero = lenarr - (upcero + downcero)
    print(f'{upcero / lenarr:.6f}')
    print(f'{downcero / lenarr:.6f}')
    print(f'{zero / lenarr:.6f}')




if __name__ == '__main__':
    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr, n)

