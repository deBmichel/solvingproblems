#!/bin/python3

import os
import sys

#Url: https://www.hackerrank.com/challenges/time-conversion/problem

#
# Complete the timeConversion function below.
#
def timeConversion(shr):
    if shr[-2]+shr[-1] == 'AM':
        baseam = ['-1','01:','02:','03:','04:','05:','06:','07:','08:','09:','10:','11:','00:']
        return baseam[int(shr[0]+shr[1])] + shr[3]+shr[4]+":" + shr[-4]+shr[-3]
    else:
        basepm = ['-1','13:','14:','15:','16:','17:','18:','19:','20:','21:','21:','23:','12:']
        return basepm[int(shr[0]+shr[1])] + shr[3]+shr[4]+":" + shr[-4]+shr[-3]
        


if __name__ == '__main__':
    #f = open(os.environ['OUTPUT_PATH'], 'w')

    print(timeConversion(input()))

    #f.write(result + '\n')

    #f.close()

