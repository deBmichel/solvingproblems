#url: https://www.hackerrank.com/challenges/staircase/problem

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the staircase function below.
def staircase(n):
	for i in range(1,n+1): #I found the solution by myself using my old solution to https://www.hackerrank.com/challenges/python-quest-1/problem 
		print(f'{" " * ((n+1-i)-1)}{"#" * i}')
			
if __name__ == '__main__':
	n = int(input())
	staircase(n)
