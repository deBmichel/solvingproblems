#!/bin/python3

#url: https://www.hackerrank.com/challenges/diagonal-difference/problem

import math
import os
import random
import re
import sys

#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#

def diagonalDifference(arr):
    i, n, primaryd, secondd = 0, len(arr) - 1, 0, 0
    while i <= n:
        primaryd += arr[i][i]
        secondd += arr[i][n-i]
        i += 1
    return abs (primaryd - secondd)

if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)
    print(result)

    #fptr.write(str(result) + '\n')

    #fptr.close()

