#!/bin/python3

import math
import os
import random
import re
import sys

"""
    Key Words : Regex for one char followed of the same char:
    Url: https://www.hackerrank.com/challenges/reduced-string/problem
"""
# Complete the superReducedString function below.
def superReducedString(s):
    pattern = re.compile(r'(.)\1')
    while pattern.search(s) != None:
        s = re.sub(r'(.)\1', '', s)
        
    if s :
        return s
    return "Empty String"

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = superReducedString(s)

    fptr.write(result + '\n')

    fptr.close()
