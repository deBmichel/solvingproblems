#!/bin/python3

#url: https://www.hackerrank.com/challenges/gem-stones/problem

import math
import os
import random
import re
import sys

# Complete the gemstones function below.
def gemstones(arr):
	base, count, tem = 'abcdefghijklmnñopqrstuvwxyz', 0, 0
	for e in base:
		for str in arr:
			if e in str:
				tem += 1
		if tem == len(arr):
			count += 1
		tem = 0
	return count

if __name__ == '__main__':
	#fptr = open(os.environ['OUTPUT_PATH'], 'w')

	n = int(input())

	arr = []

	for _ in range(n):
		arr_item = input()
		arr.append(arr_item)

	result = gemstones(arr)

	print(result)

	#fptr.write(str(result) + '\n')

	#fptr.close()
